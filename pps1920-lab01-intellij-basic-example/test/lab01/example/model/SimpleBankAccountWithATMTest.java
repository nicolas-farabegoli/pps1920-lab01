package lab01.example.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleBankAccountWithATMTest extends SimpleBankAccountTest {

    private BankAccountAtm bankAccount;
    private AccountHolder accountHolder;

    @BeforeEach
    void beforeEach() {
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccount = new SimpleBankAccountWithATM(accountHolder, 0);
        setup(accountHolder, bankAccount);
    }

    @Test
    void depositWithAtm() {
        bankAccount.depositWithAtm(accountHolder.getId(), 10);
        assertEquals(9, bankAccount.getBalance());
    }

    @Test
    void wrongDepositWithAtm() {
        bankAccount.depositWithAtm(accountHolder.getId(), 10);
        bankAccount.depositWithAtm(100, 20);
        assertEquals(9, bankAccount.getBalance());
    }

    @Test
    void withdrawWithAtm() {
        bankAccount.depositWithAtm(accountHolder.getId(), 100);
        bankAccount.withdrawWithAtm(accountHolder.getId(), 20);
        assertEquals(78, bankAccount.getBalance());
    }

    @Test
    void wrongWithdrawWithAtm() {
        bankAccount.depositWithAtm(accountHolder.getId(), 100);
        bankAccount.depositWithAtm(100, 20);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void getFee() {
        assertEquals(1, bankAccount.getFee());
    }
}