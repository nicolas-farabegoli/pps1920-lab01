package lab01.example.model;

public class SimpleBankAccountWithATM extends SimpleBankAccount implements BankAccountAtm {

    private static final double FEE = 1;

    public SimpleBankAccountWithATM(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public void depositWithAtm(final int usrId, final double amount) {
        super.deposit(usrId, amount - FEE);
    }

    @Override
    public void withdrawWithAtm(final int usrId, final double amount) {
        super.withdraw(usrId, amount + FEE);
    }

    @Override
    public double getFee() {
        return FEE;
    }
}
