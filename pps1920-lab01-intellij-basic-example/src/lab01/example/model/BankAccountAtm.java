package lab01.example.model;

public interface BankAccountAtm extends BankAccount {
    void depositWithAtm(final int usrId, final double amount);

    void withdrawWithAtm(final int usrId, final double amount);

    double getFee();
}
