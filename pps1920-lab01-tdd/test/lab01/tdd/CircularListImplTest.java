package lab01.tdd;

import lab01.tdd.factory.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
class CircularListImplTest {

    private CircularList list;

    @BeforeEach
    void beforeEach() {
        list = new CircularListImpl();
    }

    @Test
    void add() {
        IntStream.rangeClosed(0, 9).forEach(list::add);
        assertFalse(list.isEmpty());
    }

    @Test
    void size() {
        IntStream.rangeClosed(0, 9).forEach(list::add);
        assertEquals(10, list.size());
    }

    @Test
    void isEmpty() {
        assertTrue(list.isEmpty());
        list.add(0);
        assertFalse(list.isEmpty());
    }

    @Test
    void next() {
        IntStream.rangeClosed(0, 9).forEach(list::add);
        list.next();
        assertEquals(Optional.of(1), list.next());
    }

    @Test
    void nextOnPeriodic() {
        IntStream.rangeClosed(0, 9).forEach(list::add);
        IntStream.rangeClosed(0, 9).forEach(e -> list.next());
        assertEquals(Optional.of(0), list.next());
        assertEquals(Optional.of(1), list.next());
    }

    @Test
    void nextOnEmptyList() {
        assertEquals(Optional.empty(), list.next());
    }

    @Test
    void previous() {
        IntStream.rangeClosed(0, 9).forEach(list::add);
        assertEquals(Optional.of(9), list.previous());
    }

    @Test
    void previousOnEmptyList() {
        assertEquals(Optional.empty(), list.previous());
    }

    @Test
    void previousOnPeriodic() {
        IntStream.rangeClosed(0, 9).forEach(list::add);
        IntStream.rangeClosed(0, 9).forEach(e -> list.previous());
        assertEquals(Optional.of(9), list.previous());
        assertEquals(Optional.of(8), list.previous());
    }

    @Test
    void reset() {
        IntStream.rangeClosed(0, 3).forEach(list::add);
        IntStream.rangeClosed(0, 1).forEach(e -> list.next());
        list.reset();
        assertEquals(Optional.of(1), list.next());
    }

    @Test
    void nextEvenTest() {
        final var evenStrategy = FactoryProvider.getStrategy(new EvenFactory());
        IntStream.rangeClosed(0, 9).forEach(list::add);
        list.next();
        assertEquals(Optional.of(2), list.next(evenStrategy));
        assertEquals(Optional.of(4), list.next(evenStrategy));
    }

    @Test
    void multipleOfTest() {
        final var multipleOfStrategy = FactoryProvider.getStrategy(new MultipleFactory(4));
        IntStream.rangeClosed(0, 9).forEach(list::add);
        list.next();
        assertEquals(Optional.of(4), list.next(multipleOfStrategy));
        assertEquals(Optional.of(8), list.next(multipleOfStrategy));
    }

    @Test
    void multipleEqualsTest() {
        final var multipleStrategySeven = FactoryProvider.getStrategy(new EqualFactory(7));
        final var multipleStrategyZero = FactoryProvider.getStrategy(new EqualFactory(0));
        IntStream.rangeClosed(0, 9).forEach(list::add);
        list.next();
        assertEquals(Optional.of(7), list.next(multipleStrategySeven));
        assertEquals(Optional.of(0), list.next(multipleStrategyZero));
    }
}