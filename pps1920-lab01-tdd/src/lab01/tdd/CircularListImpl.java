package lab01.tdd;

import java.util.*;

public class CircularListImpl implements CircularList {

    private final List<Integer> list = new LinkedList<>();
    private Integer current = null;
    private int index = -1;

    @Override
    public void add(int element) {
        list.add(element);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (list.isEmpty()) {
            return Optional.empty();
        }
        index = nextIndex();
        current = list.get(index);
        return Optional.of(current);
    }

    @Override
    public Optional<Integer> previous() {
        if (list.isEmpty()) {
            return Optional.empty();
        }
        index = prevIndex();
        current = list.get(index);
        return Optional.of(current);
    }

    @Override
    public void reset() {
        index = 0;
        current = list.get(index);
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        List<Integer> listCopy = new LinkedList<>(list);
        index = nextIndex();
        Collections.rotate(listCopy, list.size() - index);
        var elem = listCopy.stream()
                .filter(strategy::apply)
                .findFirst();
        elem.ifPresent(e -> index = list.indexOf(e));
        return elem;
    }

    private int nextIndex() {
        if (index < 0) return 0;
        return (index + 1) % list.size();
    }

    private int prevIndex() {
        if (index <= 0) {
            return list.size() - 1;
        }
        return index - 1;
    }
}
