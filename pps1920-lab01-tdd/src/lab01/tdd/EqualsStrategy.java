package lab01.tdd;

public class EqualsStrategy implements SelectStrategy {

    private int equals;

    private EqualsStrategy() { }
    public EqualsStrategy(final int equals) {
        this.equals = equals;
    }

    @Override
    public boolean apply(int element) {
        return element == equals;
    }
}
