package lab01.tdd.factory;

import lab01.tdd.SelectStrategy;

public class FactoryProvider {
    public static SelectStrategy getStrategy(final AbstractFactory factory) {
        return factory.getStrategy();
    }
}
