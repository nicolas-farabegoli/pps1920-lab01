package lab01.tdd.factory;

import lab01.tdd.EvenStrategy;
import lab01.tdd.SelectStrategy;

public class EvenFactory extends AbstractFactory {
    @Override
    public SelectStrategy getStrategy() {
        return new EvenStrategy();
    }
}
