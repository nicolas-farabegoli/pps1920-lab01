package lab01.tdd.factory;

import lab01.tdd.MultipleStrategy;
import lab01.tdd.SelectStrategy;

public class MultipleFactory extends AbstractFactory {

    private final int multiple;

    public MultipleFactory(int multiple) {
        this.multiple = multiple;
    }

    @Override
    public SelectStrategy getStrategy() {
        return new MultipleStrategy(multiple);
    }
}
