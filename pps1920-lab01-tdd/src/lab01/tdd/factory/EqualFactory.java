package lab01.tdd.factory;

import lab01.tdd.EqualsStrategy;
import lab01.tdd.SelectStrategy;

public class EqualFactory extends AbstractFactory {

    private final int value;

    public EqualFactory(int value) {
        this.value = value;
    }

    @Override
    public SelectStrategy getStrategy() {
        return new EqualsStrategy(value);
    }
}
