package lab01.tdd.factory;

import lab01.tdd.SelectStrategy;

public abstract class AbstractFactory {
    public abstract SelectStrategy getStrategy();
}
