package lab01.tdd;

public class MultipleStrategy implements SelectStrategy {

    private int multiple;

    private MultipleStrategy() { }

    public MultipleStrategy(final int multiple) {
        this.multiple = multiple;
    }

    @Override
    public boolean apply(int element) {
        return element % multiple == 0;
    }
}
